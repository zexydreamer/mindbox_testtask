using System;

namespace mindbox_testtask.Figures
{
    public class Triangle : IFigure
    {
        private double _a { get; }
        private double _b { get; }
        private double _c { get; }
        private double _perimeter { get; }

        public Triangle(double a, double b, double c)
        {
            Exist(a, b, c);
            _a = a;
            _b = b;
            _c = c;
            _perimeter = (_a + _b + _c) / 2;
        }

        private void Exist(double a, double b, double c)
        {
            if (a <= 0 || b <= 0 || c <= 0 ||
                a >= b + c ||
                b >= a + c ||
                c >= a + b)
                throw new ArgumentException("Wrong argument");
        }

        public bool IsRightTriangle()
        {
            return Math.Abs(_c * _c - (_a * _a + _b * _b)) < .00005 ||
                   Math.Abs(_a * _a - (_b * _b + _c * _c)) < .00005 ||
                   Math.Abs(_b * _b - (_a * _a + _c * _c)) < .00005;
        }
        
        public double GetArea()
        {
            return Math.Sqrt(_perimeter*(_perimeter - _a)*(_perimeter - _b)*(_perimeter - _c));
        }
    }
}
