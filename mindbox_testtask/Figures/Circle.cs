using System;

namespace mindbox_testtask.Figures
{
    public class Circle : IFigure
    {
        private readonly double _radius;

        public Circle(double radius)
        {
            Exist(radius);
            _radius = radius;
        }

        private void Exist(double radius)
        {
            if (radius <= 0)
                throw new ArgumentException("Wrong argument");
        }

        public double GetArea() => _radius * _radius * Math.PI;
    }
}
