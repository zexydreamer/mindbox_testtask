namespace mindbox_testtask.Figures
{
    public interface IFigure
    {
        double GetArea();
    }
}