using System;
using mindbox_testtask.Figures;
using NUnit.Framework;

namespace mindbox_testtask.Tests
{
    [TestFixture]
    public class CircleTests
    {
        [TestCase(1, 1 * 1 * Math.PI)]
        public void Area_PositiveRadius_ReturnResult(double radius, double expected)
        {
            var circle = new Circle(radius);

            var actual = circle.GetArea();
            
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ZeroRadius_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new Circle(0));
        }
        
        [Test]
        public void NegativeRadius_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new Circle(-1));
        }
    }
}
