using System;
using mindbox_testtask.Figures;
using NUnit.Framework;

namespace mindbox_testtask.Tests
{
    [TestFixture]
    public class TriangleTests
    {
        [TestCase(2, 3, 4, 2.9047375096555625d)]
        public void Area_NormalEdges_ReturnResult(double a, double b, double c, double expected)
        {
            var triangle = new Triangle(a, b, c);
            
            var actual = triangle.GetArea();
            
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void IsRightTriangle_Right_ReturnTrue()
        {
            var triangle = new Triangle(3, 4, 5);
            
            var result = triangle.IsRightTriangle();
            
            Assert.IsTrue(result);
        }
        
        [Test]
        public void IsRightTriangle_NotRight_ReturnFalse()
        {
            var triangle = new Triangle(2, 4, 3);
            
            var result = triangle.IsRightTriangle();
            
            Assert.IsFalse(result);
        }
        
        [Test]
        public void ZeroEdges_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new Triangle(0, 0, 0));
        }

        [Test]
        public void NegativeEdges_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new Triangle(-1, -1, -1));
        }

        [Test]
        public void Exist_TriangleInequality_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new Triangle(1, 2, 3));
        }
    }
}
